;;; kkc-userdict.el --- User dictionary for KKC -*- lexical-binding: t -*-

;; Copyright (C) 2020 Masahiro Nakamura <tsuucat@icloud.com>

;; Author: Masahiro Nakamura <tsuucat@icloud.com>
;; Created: 2020-11-09
;; Keywords: i18n, mule, multilingual, Japanese
;; URL: https://gitlab.com/tsuu32/kkc-userdict
;; Version: 0.0.1

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; User dictionary for Kana Kanji Converter (KKC).

;; To use this package, add following code to your init file:
;;
;;  (kkc-userdict-mode)

;; To edit user dictionary, Use M-x customize-option kkc-userdict-alist
;; or custom-set-variables, not setq, append and add-to-list, etc.

;;; Code:

(require 'ja-dic-utl)
(require 'kkc)

(eval-when-compile (require 'subr-x))
(require 'cl-lib)

(defgroup kkc-userdict nil
  "KKC user dictionary."
  :group 'leim)

(defun kkc-userdict-alist--setter (symbol value)
  "The customize :set function for `kkc-userdict-alist'."
  (set-default symbol value)
  ;; Prepare kkc cache data. See `kkc-lookup-key'.
  (unless kkc-init-file-flag
    (setq kkc-init-file-flag t
          kkc-lookup-cache nil)
    (add-hook 'kill-emacs-hook 'kkc-save-init-file)
    (when (file-readable-p kkc-init-file-name)
      (condition-case nil
          (load-file kkc-init-file-name)
        (kkc-error "Invalid data in %s" kkc-init-file-name)))
    (unless (and (nested-alist-p kkc-lookup-cache)
		 (eq (car kkc-lookup-cache) kkc-lookup-cache-tag))
      (setq kkc-lookup-cache (list kkc-lookup-cache-tag)
            kkc-init-file-flag 'kkc-lookup-cache)))
  ;; Remove cache entry if exist.
  (dolist (e value)
    (let ((yomi-seq (vconcat (car e))))
      (when (lookup-nested-alist yomi-seq kkc-lookup-cache nil 0 t)
        (set-nested-alist yomi-seq nil kkc-lookup-cache)))))

(defcustom kkc-userdict-alist '()
  "User dictionary for KKC.

Each element is a list comprising (よみ 変換...)

よみ is hiragana reading of the word.  変換 is the replacement word.
A よみ may have multiple replacement words.

Use \\[customize] or `custom-set-variables' to edit this option, not
`setq', `append' and `add-to-list', etc. because KKC conversion is
cached in `kkc-lookup-cache'."
  :type '(alist :key-type (string :tag "よみ")
                :value-type (repeat :tag "変換" (string :tag "変換")))
  :set #'kkc-userdict-alist--setter)

(defun kkc-userdict--skkdic-lookup-key (orig-fun seq &rest args)
  "Advice function for `skkdic-lookup-key'."
  (append
   ;; Prioritize userdict entry.
   (let ((yomi (concat seq)))
     (cdr (assoc yomi kkc-userdict-alist #'string-equal)))
   (apply orig-fun seq args)))

;;;###autoload
(define-minor-mode kkc-userdict-mode
  "Toggle user dictionary for KKC.

Built-in japanese input method uses KCC for Kana Kanji Convertion and
KKC doesn't provide user dictionary feature.

KKC Userdict mode is a global minor mode that add user dictionary
feature for KKC.

`kkc-userdict-alist' is the user dictionary used when KKC Userdict mode
is enabled.  Customize the option to add or remove words in user
dictionary."
  :global t
  (if kkc-userdict-mode
      ;; Enable
      (progn
        (advice-add 'skkdic-lookup-key :around
                    #'kkc-userdict--skkdic-lookup-key)
        ;; Remove cache entry if exist.
        (dolist (e kkc-userdict-alist)
          (let ((yomi-seq (vconcat (car e))))
            (when (lookup-nested-alist yomi-seq kkc-lookup-cache nil 0 t)
              (set-nested-alist yomi-seq nil kkc-lookup-cache)))))
    ;; Disable
    (advice-remove 'skkdic-lookup-key #'kkc-userdict--skkdic-lookup-key)))

;;; KKC Userdict menu mode

(defvar kkc-userdict-menu-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map [?d] 'kkc-userdict-menu-delete-entry)
    map)
  "Local keymap for `kkc-userdict-menu-mode' buffers.")

(define-derived-mode kkc-userdict-menu-mode tabulated-list-mode "KKC Userdict"
  "Major mode for listing the KKC user dictionary."
  (setq tabulated-list-format [("よみ" 15 t)
                               ("変換"   10 t)])
  (setq tabulated-list-sort-key (cons "よみ" nil))
  (add-hook 'tabulated-list-revert-hook #'kkc-userdict-menu--refresh nil t)
  (tabulated-list-init-header))

(defun kkc-userdict-menu-delete-entry ()
  "Remove entry at point from `kkc-userdict-alist'."
  (interactive)
  (if-let ((e (tabulated-list-get-id)))
      (when (y-or-n-p "Delete this entry? ")
        (let* ((yomi (car e))
               (henkan (cdr e))
               (henkans
                (cdr (assoc yomi kkc-userdict-alist #'string-equal))))
          (if (eq 1 (length henkans))
              (setq kkc-userdict-alist
                    (assoc-delete-all yomi kkc-userdict-alist #'string-equal))
            (setf (cdr (assoc yomi kkc-userdict-alist #'string-equal))
                  (cl-remove henkan henkans :test #'string-equal)))
          ;; Remove cache entry if exist.
          (let ((yomi-seq (vconcat yomi)))
            (when (lookup-nested-alist yomi-seq kkc-lookup-cache nil 0 t)
              (set-nested-alist yomi-seq nil kkc-lookup-cache))))
        (let ((pos (point)))
          (revert-buffer)
          (goto-char (min pos (point-max)))
          (if (eobp)
              (forward-line -1)
            (beginning-of-line))))
    (user-error "No entry")))

(defun kkc-userdict-menu--refresh ()
  "Refresh the words in user dictionary for the KKC Userdict Menu buffer."
  (setq tabulated-list-entries nil)
  (dolist (e kkc-userdict-alist)
    (let ((yomi (car e))
          (henkans (cdr e)))
      (dolist (h henkans)
        (push (list (cons yomi h)
                    `[,yomi ,h])
              tabulated-list-entries)))))

;;;###autoload
(defun kkc-userdict-list-userdict ()
  "Display the KKC user dictionary.
Words are displayed in a buffer named *KKC Userdict*."
  (interactive)
  (let ((buf (get-buffer-create "*KKC Userdict*")))
    (with-current-buffer buf
      (kkc-userdict-menu-mode)
      (kkc-userdict-menu--refresh)
      (tabulated-list-print))
    (switch-to-buffer buf)))

(provide 'kkc-userdict)

;;; kkc-userdict.el ends here
